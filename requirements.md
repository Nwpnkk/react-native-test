
# After clone project

- npm install or yarn install
- react-native link
- cd ios pod install


# Test Requirements

- # หน้า Main Screen
  - เรียก API GET - {config.baseUrl}/products/test
  - response data ที่ได้ให้ display เป็นรูปแบบ product card  (สร้าง component product card ขึ้นมาได้เลย)
  - ดู design หน้า Main screen ได้ที่ link https://www.canva.com/design/DAEou0cB-4w/d06fC09P87xqYycicmyzug/view?utm_content=DAEou0cB-4w&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton
  - สามารถ click ที่ product card เพื่อ navigate ไปหน้า Product Screen 
  - image product หน้า product card ให้ใช้ { uri : {config.imageUrl} + "/" + product.img_url.slice(0,-4) + "-1-n.jpg" }
  - ส่ง param 2 อย่างไปหน้า Product Screen คือ category และ sku


- # หน้า Product Screen
  - design Product detail โดยมีรูปสินค้า ชื่อ brand , name , sku
  - สร้างปุ่มให้สามารถกดแล้วไปหน้า Listing Screen เพื่อ create สินค้าขึ่นมาใหม่
  - ส่งข้อมูล product เพื่อ navigate ไปหน้า Listing Screen

- # หน้า Listing Screen
  - สร้าง form ในการสร้าง order สำหรับตั้งขายสินค้าขึ้นมาใหม่โดยมีข้อมูลที่ต้องกรอกดังนี้
    - สภาพสินค้า -> brand_new | like_new | well_used
    - ไซต์สินค้า -> 3.5 - 14
    - ราคา : price
    - asker_email : string
    - asker : uniqueId 
    - details :object
      # เงื่อนไขของ details
        - ให้เลือกโดย radiobox เงื่อนไขเดียว
        - ตัวอย่าง
          option DETAILS_BOX = { DETAILS_BOX : true }
          option DETAILS_BOX_DEFECT = { DETAILS_BOX_DEFECT : true }
          option DETAILS_NO_BOX = { DETAILS_BOX : false }
      
  - ลักษณะ oject payload
        payload = {
            asker : (string), ใช้ library uuid
            asker_email : email (string), กรอกจาก textinput
            bidder : "",
            bidder_email : "",
            category : product.category,
            condition : string one of  brand_new | like_new | well_used , เลือกจาก dropdown
            product : {
                ...product data ที่มาจาก params หน้า Product Screen
            },

            shipping_from : {
                address : string,      กรอกจาก textinput
                district : string,     กรอกจาก textinput
                state : string,        กรอกจาก textinput
                province : string,     กรอกจาก textinput
                postcode : string,     กรอกจาก textinput
                tel : string           กรอกจาก textinput
            },
            shipping_to : "",
            details : {},   เลือกจาก radiobox 
            services : {},
            country : "",
            price : number,   กรอกจาก textinput
            size : string,
            sku: product.sku,
            international: "",
            imgUpload : {},
            defectUpload : {},
            status : "test",
            type: "ask"
        }
    - ส่ง payload มาที่ API -  POST {config.baseUrl}/orders/test
    - เมื่อ response == 200 ให้ show alert success
    - เมื่อ กด ok ให้กลับมาหน้า Main Screen

 

    