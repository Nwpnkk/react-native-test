import React, { Component } from 'react'
import { Platform, NativeModules } from 'react-native'
import Styled from 'styled-components'
import { StatusBar } from 'react-native'
import { colors } from '../src/utils'
import AppNavigator from './AppNavigator'

const MainApp = () => {
  return (
    <AppNavigator />
  )
}
export default MainApp

