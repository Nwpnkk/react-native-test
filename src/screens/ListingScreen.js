import React from 'react'
import { View, Text } from 'react-native'
import Container from '../components/Container'

const index = (props) => {
    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text fontSize={16} color={"black"}>Listing Screen</Text>
        </View>
    )
}

export default index