const colors = {
    backgroundPrimary: '#ffffff',
    backgroundSecondary: '#ececec',
    white: '#ffffff',
    gray: '#dfdfdf',
    light: "#f9f9f9",
    lightGray: '#d7d7d7',
    textGray: "#7a7a7a",
    darkGray: '#434343',
    lightBlack: "#242424",
    black: '#000000',
    blackTransparent: "rgba(0,0,0,0.2)",
    lightTransparent: "rgba(255,255,255,0.5)",
    softGreen: "#8AC0BA",
    lighGreen: "#43d3a3",
    lightVintageGreen: "#93d4cd",
    vintageGreen: "#79c2ba",
    facebook: "#475993",
    lightRed: "#ff3722",
    red: "#f44336",
    vintageRed: '#c27981',
    lightVintageRed: "#da949b",
    softRed: "#FFDDDD",
    yellow: "#FFC327",
    transparent: "rgba(0,0,0,0)",
    //picker
    pickerBackground: "#232325",
    pickerHeader: "#202023",
    pickerLabel: "#0674f2",
    pickerItem: "#a9a9ad",
    // graph
    graphArea: "#8AC0BA",

}

export default colors