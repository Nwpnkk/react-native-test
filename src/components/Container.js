import React from 'react'
import { View } from 'react-native'
import Styled from 'styled-components'

const Container = Styled(View)`
    flex: 1;
    alignItems: center;
    backgroundColor: white;
`
export default Container