import React, { useEffect, useState, useMemo } from 'react'
import { View, LogBox, Image } from 'react-native'
import Styled from 'styled-components'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'
import { heightPercentageToDP as vh, widthPercentageToDP as vw } from 'react-native-responsive-screen'

import MainScreen from './src/screens/MainScreen'
import ListingScreen from './src/screens/ListingScreen'
import OrdersScreen from './src/screens/OrdersScreen'
import ProductScreen from './src/screens/ProductScreen'

const Stack = createStackNavigator()
const BottomTab = createBottomTabNavigator()

const Container = Styled(View)`
    flex: 1;
`
const IconImage = Styled(Image)`
    resizeMode: contain;
    width: 100px;
    height: 100px;
`
const AppStackNavigator = () => useMemo(() => (
    <Stack.Navigator initialRouteName="MainApp">
        <Stack.Screen name={"MainApp"} component={BottomStackNavigator} />
        <Stack.Screen name={"Listing"} component={ListingScreen} />
        <Stack.Screen name={"Product"} component={ProductScreen} />
    </Stack.Navigator>
))

const BottomStackNavigator = () => useMemo(() => (
    <BottomTab.Navigator initialRouteName="Main"  >
        <Stack.Screen name={"Main"} component={MainScreen} />
        <Stack.Screen name={"Orders"} component={OrdersScreen} />
    </BottomTab.Navigator>
))

const AppNavigator = () => {
    return (
        <NavigationContainer>
            <AppStackNavigator />
        </NavigationContainer>
    )
}

export default function (props) {
    return (
        <Container>
            <AppNavigator {...props} />
        </Container>
    )
}
